package com.country;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadSomeCountries {
    private static final Logger log = LoggerFactory.getLogger(LoadSomeCountries.class);

    @Bean
    CommandLineRunner initDatabase(CountryRepository repository) {
  
      return args -> {
        log.info("Preloading " + repository.save(new Country("France", "FR")));
        log.info("Preloading " + repository.save(new Country("United Kingdom", "UK")));
      };
    }
    
}
