package com.country;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;




@CrossOrigin(origins = "http://localhost:8080/")
@RestController
public class CountryController {
    private final CountryRepository repository;

    CountryController(CountryRepository repository) {
      this.repository = repository;
    }
    @GetMapping("/countries")
    List<Country> all() {
      return repository.findAll();
    }

  
    @PostMapping("/countries")
    Country newCountry(@RequestBody Country newEmployee) {
      return repository.save(newEmployee);
    }
  

    @GetMapping("/country/{id}") public ResponseEntity<Country> getCountry(@PathVariable Long id) { 
      var value = repository.findById(id); 
      if(value.isEmpty()) { var c = new Country(); 
        c.setId(null); 
        c.setName("ERROR"); 
        c.setAbbreviation(String.format("Impossible d'obtenir le pays portant l'identifiant %s", id)); 
        return new ResponseEntity<>(c, HttpStatus.NOT_FOUND); } 
        else { 
      var c = value.get(); return new ResponseEntity<>(c, HttpStatus.FOUND);}
    }
    // @GetMapping("/countries/{id}")
    // public ResponseEntity<Country>getCountry(@PathVariable Long id)  {


    //     var value = repository.findById(id);

    //     if(value.isEmpty()) {
    //         var c = new Country();
    //         c.setId(null);
    //         c.setName("ERROR");
    //         c.setAbbreviation(String.format("Impossible d'obtenir le pays portant l'identifiant %s", id));
    //         return new ResponseEntity<>(
    //           c, HttpStatus.NOT_FOUND);
    //     };
 
      
    //   // return repository.findById(id)
    //   //   .orElseThrow(() -> new CountryNotFoundException(id));
        
    // }

    @PutMapping("/country/{id}")
    Country replaceCountry(@RequestBody Country newCountry, @PathVariable Long id) {
      
      return repository.findById(id)
        .map(country -> {
          country.setName(newCountry.getName());
          country.setAbbreviation(newCountry.getAbbreviation());
          return repository.save(country);
        })
        .orElseGet(() -> {
          newCountry.setId(id);
          return repository.save(newCountry);
        });
    }
  
    @DeleteMapping("/country/{id}")
    void deleteCountry(@PathVariable Long id) {
      repository.deleteById(id);
    }

}
