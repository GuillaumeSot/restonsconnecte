package com.country;

public class CountryNotFoundException extends RuntimeException {
    CountryNotFoundException(Long id) {
        super("Could not find country " + id);
      }
    
}
