package com.country;
import org.springframework.data.jpa.repository.JpaRepository;



interface CountryRepository extends JpaRepository<Country, Long> {

} 

