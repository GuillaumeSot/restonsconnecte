package com.country;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Country {
    private @Id @GeneratedValue Long id;
    private String name;
    private String abbreviation;


    Country() {}

    Country(String name, String abbreviation) {
  
      this.name = name;
      this.abbreviation = abbreviation;
    }
  
    public Long getId() {
      return this.id;
    }
  
    public String getName() {
      return this.name;
    }
  
    public String getAbbreviation() {
      return this.abbreviation;
    }
  
    public void setId(Long id) {
      this.id = id;
    }
  
    public void setName(String name) {
      this.name = name;
    }
  
    public void setAbbreviation(String abbreviation) {
      this.abbreviation = abbreviation;
    }
}
