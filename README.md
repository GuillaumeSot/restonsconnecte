# restonsConnecté

But du projet:
Ce projet a pour but de faire une api REST avec différents pays contenant leurs noms et leurs codes, et de leurs appliqués un CRUD. Ainsi que faire une page HTML/CSS/JS afin d'effectuer des recherches via un formulaire qui retournera les résultats.


Prérequis d'installation:

-Java 11

-Le framework SpringBoot


Commandes intéressantes:

-mvn spring-boot:run

-mvn clean

Liste des urls + commandes CURL:

GET /countries => La liste de tous les pays "curl -v localhost:8080/countries/"

GET /country/{id} => Les informations du pays portant cet id "curl -v localhost:8080/country/1"

POST /country => Enregistre un pays "curl -X POST localhost:8080/countries -H 'Content-type:application/json' -d '{"name": "france", "abbreviation": "FR"}'"

PUT /country/{id} => Met à jour un pays portant cet id "curl -X PUT localhost:8080/country/1 -H "Content-Type: application/json" -d '{"name":"france", "code":"FR"}'"

DELETE /country/{id} => Supprime le pays portant cet id "curl -X DELETE localhost:8080/country/1"


Configuration de la base de données dans "demo/src/main/ressources/application.properties"



Pour générer le jar, la commande est  "./mvnw clean package"
Pour le lancer, la commande est "java -jar target/demo-0.0.1-SNAPSHOT.jar"



L'application se lance sur "http://localhost:8080/" 








